﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace SmartQueue.Repository
{
    public static class DbSetExtensions
    {
        public static void RemoveByCondition<TEntity>(this DbSet<TEntity> table, Expression<Func<TEntity, bool>> predicate)
            where TEntity : class
        {
            table.Remove(table.FirstOrDefault(predicate));
        }
    }
}