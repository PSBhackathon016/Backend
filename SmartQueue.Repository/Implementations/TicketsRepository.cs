﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SmartQueue.DataAccess;
using SmartQueue.Enums;
using SmartQueue.Repository.Interfaces;

namespace SmartQueue.Repository.Implementations
{
    public class TicketsRepository : ITicketsRepository
    {
        public List<Ticket> GetTicketsByFilter(int branchId, int operationId, DateTime begin, DateTime end)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                return
                    dataContext.Tickets.Where(x => x.BranchId == branchId 
                    && x.OperationId == operationId 
                    && x.DateEvent >= begin 
                    && x.DateEvent < end
                    && x.State == TicketStatus.Draft)
                        .Include(x => x.Branch)
                        .Include(x=>x.Operation)
                        .Include(x=>x.Client)
                        .ToList();
            }
        }

        public Ticket GetById(int id)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                return
                    dataContext.Tickets
                        .Include(x => x.Branch)
                        .Include(x => x.Operation)
                        .Include(x => x.Client)
                        .FirstOrDefault(x=>x.Id == id);
            }
        }

        public void Reserve(int id, int clientId, string number)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                
                Ticket ticket = dataContext.Tickets.FirstOrDefault(x => x.Id == id);

                if(ticket == null)
                    throw new NullReferenceException("Не найден клиент по id");

                ticket.ClientId = clientId;
                ticket.Number = number;
                ticket.State = TicketStatus.Complite;

                dataContext.SaveChanges();
            }
        }

        public List<Ticket> GetTicketsByClient(int clientId)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                return
                    dataContext.Tickets.Where(x => x.ClientId == clientId && x.State == TicketStatus.Complite)
                        .Include(x => x.Branch)
                        .Include(x => x.Operation)
                        .Include(x => x.Client).ToList();
            }
        }
    }
}