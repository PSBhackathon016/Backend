﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SmartQueue.DataAccess;
using SmartQueue.Repository.Interfaces;

namespace SmartQueue.Repository.Implementations
{
    public class BranchesRepository : IBranchesRepository
    {
        //public List<Branch> GetAllBranches()
        //{
        //    using (DbDataContext dataContext = new DbDataContext())
        //    {
        //        return dataContext.Branches.ToList();
        //    }
        //}

        public List<CongestionBranch> GetCongestionBranches(int branchId, DateTime date)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                DateTime dateBegin = date.Date.AddHours(8);
                DateTime dateEnd = date.Date.AddHours(20);
                return
                    dataContext.CongestionBranches.Where(
                        x => x.BranchId == branchId && x.DateEvent >= dateBegin && x.DateEvent < dateEnd).ToList();
            }
        }

        //public List<Branch> GetAllBranches(DateTime date)
        //{
        //    int dayNumber = this.ConvertToDayNumber(date.DayOfWeek);
        //    List<Branch> result = new List<Branch>();
        //    using (DbDataContext dataContext = new DbDataContext())
        //    {
        //        List<Branch> branches =
        //            dataContext.Branches.Where(x => true)
        //                .Include(x => x.WorkTimeBranches)
        //                .Include(x => x.BranchesToOperations)
        //                .ToList();

        //        foreach (Branch branch in branches)
        //        {
        //            foreach (var VARIABLE in branch.WorkTimeBranches)
        //            {
                        
        //            }
        //        }
        //    }
        //}

        public int ConvertToDayNumber(DayOfWeek usaDay)
        {
            switch (usaDay)
            {
                case 0:
                    return 7;
                default:
                    return (int)usaDay;
            }
        }

        public List<Branch> GetAllBranchesByFilter(DateTime date, int operationId)
        {
            int dayNumber = this.ConvertToDayNumber(date.DayOfWeek);
            List<Branch> result = new List<Branch>();
            TimeSpan timeOfDay = date.TimeOfDay;
            using (DbDataContext dataContext = new DbDataContext())
            {
                List<Branch> branches =
                    dataContext.Branches.Where(x => true)
                        .Include(x => x.WorkTimeBranches)
                        .Include(x => x.BranchesToOperations)
                        .ToList();

                foreach (Branch branch in branches)
                {
                    foreach (BranchesToOperation operation in branch.BranchesToOperations)
                    {
                        if (operation.OperationId == operationId)
                        {
                            foreach (WorkTimeBranch timeBranch in branch.WorkTimeBranches.Where(x=>x.WeekDay == dayNumber))
                            {
                                if (timeBranch.BeginTime.TimeOfDay <= timeOfDay && timeBranch.EndTime.TimeOfDay > timeOfDay
                                    && (timeBranch.LunchBeginTime.HasValue && timeBranch.LunchBeginTime.Value.TimeOfDay < timeOfDay || !timeBranch.LunchBeginTime.HasValue)
                                    && (timeBranch.LunchEndTime.HasValue && timeBranch.LunchEndTime.Value.TimeOfDay > timeOfDay || !timeBranch.LunchEndTime.HasValue))
                                {
                                    result.Add(branch);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}