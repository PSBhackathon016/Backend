﻿using System.Collections.Generic;
using System.Linq;
using SmartQueue.DataAccess;
using SmartQueue.Repository.Interfaces;

namespace SmartQueue.Repository.Implementations
{
    public class OperationsRepository : IOperationsRepository
    {
        public List<Operation> GetAllOperations()
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                return dataContext.Operations.Include(nameof(Operation.BranchesToOperations)).ToList();
            }
        }

        public bool IsFavoriteOperation(int clientId, int operationId)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                return dataContext.FavoriteOperations.Any(r => r.ClientId == clientId && r.OperationId == operationId);
            }
        }

        public void AddFavoriteOperation(int clientId, int operationId)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                FavoriteOperation favoriteOperation = new FavoriteOperation
                {
                    ClientId = clientId,
                    OperationId = operationId
                };

                dataContext.FavoriteOperations.Add(favoriteOperation);
                dataContext.SaveChanges();
            }
        }

        public void RemoveFavoriteOperation(int clientId, int operationId)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                dataContext.FavoriteOperations.RemoveByCondition(r => r.ClientId == clientId && r.OperationId == operationId);
                dataContext.SaveChanges();
            }
        }

        public List<Document> GetOperationDocuments(int operationId)
        {
            using (DbDataContext dataContext = new DbDataContext())
            {
                return dataContext.OperationsToDocuments.Where(r => r.OperationId == operationId).Select(r => r.Document).ToList();
            }
        }
    }
}