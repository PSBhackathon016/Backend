﻿using System;
using System.Collections.Generic;
using SmartQueue.DataAccess;

namespace SmartQueue.Repository.Interfaces
{
    public interface IBranchesRepository
    {
        //List<Branch> GetAllBranches(DateTime date);

        List<CongestionBranch> GetCongestionBranches(int branchId, DateTime date);

        List<Branch> GetAllBranchesByFilter(DateTime date, int operationId);
    }
}