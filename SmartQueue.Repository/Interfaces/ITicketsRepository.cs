﻿using System;
using System.Collections.Generic;
using SmartQueue.DataAccess;

namespace SmartQueue.Repository.Interfaces
{
    public interface ITicketsRepository
    {
        List<Ticket> GetTicketsByFilter(int branchId, int operationId, DateTime begin, DateTime end);

        Ticket GetById(int id);

        void Reserve(int id, int clientid, string number);

        List<Ticket> GetTicketsByClient(int clientId);
    }
}