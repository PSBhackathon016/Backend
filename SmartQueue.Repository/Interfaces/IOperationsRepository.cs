using System.Collections.Generic;
using SmartQueue.DataAccess;

namespace SmartQueue.Repository.Interfaces
{
    public interface IOperationsRepository
    {
        List<Operation> GetAllOperations();

        bool IsFavoriteOperation(int clientId, int operationId);

        void AddFavoriteOperation(int clientId, int operationId);

        void RemoveFavoriteOperation(int clientId, int operationId);

        List<Document> GetOperationDocuments(int operationId);
    }
}