﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartQueue.DataAccess;
using SmartQueue.Models;
using SmartQueue.Repository.Implementations;
using SmartQueue.Repository.Interfaces;

namespace SmartQueue.Logic
{
    public class OperationsLogic
    {
        private readonly IOperationsRepository _operationsRepository;

        public OperationsLogic()
        {
            this._operationsRepository = new OperationsRepository();
        }

        public List<OperationModel> GetAllOperations(int clientId)
        {
            List<OperationModel> operationModels = new List<OperationModel>();
            List<Operation> operations = this._operationsRepository.GetAllOperations();
            foreach (Operation operation in operations)
            {
                List<string> documentNames = this._operationsRepository.GetOperationDocuments(operation.Id).Select(r => r.Name).ToList();
                OperationModel operationModel = new OperationModel();
                operationModel.Id = operation.Id;
                operationModel.Name = operation.Name;
                operationModel.Description = operation.Description;
                operationModel.Documents = string.Join(", ", documentNames);
                operationModel.Favorite = this._operationsRepository.IsFavoriteOperation(clientId, operation.Id);
                operationModel.Offices = operation.BranchesToOperations.Select(r => r.BranchId).ToList();
                operationModels.Add(operationModel);
            }

            return operationModels;
        }

        public void AddFavoriteOperation(int clientId, int operationId)
        {
            this._operationsRepository.AddFavoriteOperation(clientId, operationId);
        }

        public void RemoveFavoriteOperation(int clientId, int operationId)
        {
            this._operationsRepository.RemoveFavoriteOperation(clientId, operationId);
        }
    }
}