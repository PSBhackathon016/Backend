﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using SmartQueue.DataAccess;
using SmartQueue.Enums;
using SmartQueue.Models;
using SmartQueue.Models.Converters;
using SmartQueue.Repository.Implementations;
using SmartQueue.Repository.Interfaces;

namespace SmartQueue.Logic
{
    public class TicketsLogic
    {
        public ITicketsRepository _ticketsRepository;
        
        public TicketsLogic()
        {
            this._ticketsRepository = new TicketsRepository();
        }

        public List<TicketModel> GetTicketsByClient(int clientId)
        {
            List<Ticket> tickets = this._ticketsRepository.GetTicketsByClient(clientId);
            List<TicketModel> models = ConvertToModels(tickets);
            return models;
        }

        public List<TicketModel> GetTicketsByFilter(int branchId, int operationId, DateTime begin, DateTime end)
        {
            List<Ticket> tickets = this._ticketsRepository.GetTicketsByFilter(branchId, operationId, begin, end);
            List<TicketModel> models = ConvertToModels(tickets);
            return models;
        }

        public void Reserve(int id, int clientId)
        {
            string number = GetNumber(clientId);
            this._ticketsRepository.Reserve(id, clientId, number);
        }

        private string GetNumber(int id)
        {
            return $"AE{id}";
        }

        private List<TicketModel> ConvertToModels(IEnumerable<Ticket> tickets)
        {
            List<TicketModel> models = new List<TicketModel>();
            foreach (Ticket ticket in tickets)
            {
                TicketModel model = ConvertToModel(ticket);

                models.Add(model);
            }

            return models;
        }

        private TicketModel ConvertToModel(Ticket ticket)
        {
            TicketModel model = new TicketModel()
            {
                BranchId = ticket.BranchId,
                BranchName = ticket.Branch.Name,
                ClientId = ticket.ClientId,
                DateEvent = ticket.DateEvent.ToUnixStamp(),
                OperationId = ticket.OperationId,
                OperationName = ticket.Operation.Name,
                State = ticket.State,
                Id = ticket.Id,
                Number = ticket.Number,
                

            };

            if (ticket.ClientId.HasValue)
            {
                model.ClientName = $"{ticket.Client.FirstName} {ticket.Client.LastName}";
            }

            return model;
        }

        public TicketModel GetById(int id)
        {

            Ticket ticket = this._ticketsRepository.GetById(id);
            TicketModel model = ConvertToModel(ticket);
            return model;
        }
    }
}