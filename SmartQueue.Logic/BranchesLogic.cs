﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Device.Location;
using System.Globalization;
using System.Linq;
using SmartQueue.DataAccess;
using SmartQueue.Models;
using SmartQueue.Models.Converters;
using SmartQueue.Repository.Implementations;
using SmartQueue.Repository.Interfaces;

namespace SmartQueue.Logic
{
    public class BranchesLogic
    {
        private readonly IBranchesRepository _branchesRepository;
        private readonly ITicketsRepository _ticketsRepository;

        public BranchesLogic()
        {
            this._branchesRepository = new BranchesRepository();
            this._ticketsRepository = new TicketsRepository();
        }

        public List<BranchCongestionModel> GetBranchesCongestion(DateTime date, double longitude, double latitude, int operationId)
        {
            List<BranchCongestionModel> result = new List<BranchCongestionModel>();

            GeoCoordinate userCoordinate = new GeoCoordinate(latitude, longitude);
            List<Branch> branches = this._branchesRepository.GetAllBranchesByFilter(date, operationId);
            foreach (Branch branch in branches)
            {
                List<CongestionBranch> hours = GetHours(branch.Id, date).Where(x=>x.State != 0).ToList();
                BranchCongestionModel model = new BranchCongestionModel();
                model.Id = branch.Id;
                model.Name = branch.Name;
                model.Longitude = branch.Coordinate.Longitude.Value;
                model.Latitude = branch.Coordinate.Latitude.Value;
                model.Distance = userCoordinate.GetDistanceTo(new GeoCoordinate(branch.Coordinate.Latitude.Value, branch.Coordinate.Longitude.Value));
                model.Hours = this.ConvertToModels(hours, operationId);
                model.Congestion = this.CalcCongestion(hours);
                result.Add(model);
            }

           // result.ForEach(r => r.Congestion = (int)r.Hours.Average(h => h.Congestion));
            return result.OrderBy(r => r.Distance).ToList();
        }

        private int CalcCongestion(List<CongestionBranch> hours)
        {
            if (!hours.Any())
                return 0;

            int congestion = (int)hours.Average(h => h.State);

            if (congestion == 0)
                return 0;

            return TenToTree(congestion);
        }

        private static int TenToTree(int congestion)
        {
            if (congestion < 4)
                return 1;

            if (congestion >= 4 && congestion < 7)
                return 2;

            if (congestion >= 7)
            {
                return 3;
            }

            return 1;
        }

        private List<CongestionBranch> GetHours(int branchId, DateTime date)
        {
            return this._branchesRepository.GetCongestionBranches(branchId, date);
        }

        private List<HourCongestionModel> ConvertToModels(List<CongestionBranch> congestionBranches, int operationId)
        {
            List<HourCongestionModel> models = new List<HourCongestionModel>();
            foreach (CongestionBranch congestionBranch in congestionBranches)
            {
                HourCongestionModel model = ConvertToModel(congestionBranch, operationId);
                models.Add(model);
            }

            return models;
        }

        private HourCongestionModel ConvertToModel(CongestionBranch congestionBranch, int operationId)
        {
            DateTime beginDate = congestionBranch.DateEvent;
            DateTime endDate = congestionBranch.DateEvent.AddHours(2);

            HourCongestionModel model = new HourCongestionModel();
            model.Congestion = TenToTree(congestionBranch.State);
            model.StartTime = beginDate.ToUnixStamp();
            model.EndTime = endDate.ToUnixStamp();
            model.CountTickets =
                this._ticketsRepository.GetTicketsByFilter(congestionBranch.BranchId, operationId, beginDate, endDate).Count;
            return model;
        }

        private static DbGeography CreateDbGeography(double longitude, double latitude)
        {
            string point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", longitude, latitude);
            return DbGeography.PointFromText(point, 4326);
        }
    }
}