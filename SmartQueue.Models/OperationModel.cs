﻿using System.Collections.Generic;

namespace SmartQueue.Models
{
    public class OperationModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Documents { get; set; }

        public bool Favorite { get; set; }

        public List<int> Offices { get; set; }
    }
}