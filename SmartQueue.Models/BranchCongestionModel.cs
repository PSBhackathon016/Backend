﻿using System.Collections.Generic;

namespace SmartQueue.Models
{
    public class BranchCongestionModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public double Distance { get; set; }

        public int Congestion { get; set; }

        public List<HourCongestionModel> Hours { get; set; }
    }
}