﻿using System;

namespace SmartQueue.Models
{
    public class HourCongestionModel
    {
        public long StartTime { get; set; }

        public long EndTime { get; set; }

        public int Congestion { get; set; }

        public int CountTickets { get; set; }
    }
}