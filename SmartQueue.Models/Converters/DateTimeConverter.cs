﻿using System;
using System.Threading;

namespace SmartQueue.Models.Converters
{
    public static class DateTimeConverter
    {
        private static DateTime _unixDate = new DateTime(1970, 01, 01, 0, 0, 0);

        public static DateTime ToDateTime(this long millisecinds)
        {
            return _unixDate.AddMilliseconds(millisecinds);
        }

        public static long ToUnixStamp(this DateTime date)
        {
            double totalMilliseconds = (date - _unixDate).TotalMilliseconds;
            return Convert.ToInt64(totalMilliseconds);
        }
    }
}