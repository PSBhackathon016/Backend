﻿using Newtonsoft.Json;
using SmartQueue.Enums;

namespace SmartQueue.Models
{
    public class TicketModel
    {
        public int Id { get; set; } 

        public string Number { get; set; } 

        public long DateEvent { get; set; } 

        public int? ClientId { get; set; }

        public string ClientName { get; set; }

        [JsonProperty(PropertyName = "OfficeId")]
        public int BranchId { get; set; }

        [JsonProperty(PropertyName = "OfficeName")]
        public string BranchName { get; set; }

        public int OperationId { get; set; }

        public string OperationName { get; set; }

        public TicketStatus State { get; set; } 
    }
}