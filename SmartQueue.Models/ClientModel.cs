﻿namespace SmartQueue.Models
{
    public class ClientModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }
    }
}